//================= Hercules Database =====================================
//=       _   _                     _
//=      | | | |                   | |
//=      | |_| | ___ _ __ ___ _   _| | ___  ___
//=      |  _  |/ _ \ '__/ __| | | | |/ _ \/ __|
//=      | | | |  __/ | | (__| |_| | |  __/\__ \
//=      \_| |_/\___|_|  \___|\__,_|_|\___||___/
//================= License ===============================================
//= This file is part of Hercules.
//= http://herc.ws - http://github.com/HerculesWS/Hercules
//=
//= Copyright (C) 2018  Hercules Dev Team
//=
//= Hercules is free software: you can redistribute it and/or modify
//= it under the terms of the GNU General Public License as published by
//= the Free Software Foundation, either version 3 of the License, or
//= (at your option) any later version.
//=
//= This program is distributed in the hope that it will be useful,
//= but WITHOUT ANY WARRANTY; without even the implied warranty of
//= MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//= GNU General Public License for more details.
//=
//= You should have received a copy of the GNU General Public License
//= along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=========================================================================
//= Pets Database
//=========================================================================

pet_db:(
/**************************************************************************
 ************* Entry structure ********************************************
 **************************************************************************
{
	// ================ Mandatory fields ==============================
	Id: ID                               (int)
	SpriteName: "Sprite_Name"            (string)
	Name: "Pet Name"                     (string)
	// ================ Optional fields ===============================
	TamingItem: Taming Item              (string, defaults to 0)
	EggItem: Egg Id                      (string, defaults to 0)
	AccessoryItem: Equipment Id          (string, defaults to 0)
	FoodItem: Food Id                    (string, defaults to 0)
	FoodEffectiveness: hunger points     (int, defaults to 0)
	HungerDelay: hunger time             (int, defaults to 0)
	Intimacy: {
		Initial: start intimacy                   (int, defaults to 0)
		FeedIncrement: feeding intimacy           (int, defaults to 0)
		OverFeedDecrement: overfeeding intimacy   (int, defaults to 0)
		OwnerDeathDecrement: owner die intimacy   (int, defaults to 0)
	}
	CaptureRate: capture rate            (int, defaults to 0)
	Speed: speed                         (int, defaults to 0)
	SpecialPerformance: true/false       (boolean, defaults to false)
	TalkWithEmotes: convert talk         (boolean, defaults to false)
	AttackRate: attack rate              (int, defaults to 0)
	DefendRate: Defence attack           (int, defaults to 0)
	ChangeTargetRate: change target      (int, defaults to 0)
	PetScript: <" Pet Script (can also be multi-line) ">
	EquipScript: <" Equip Script (can also be multi-line) ">
},
**************************************************************************/
	// entries in this file will override the ones in /(pre-)re/pet_db.conf
{
	Id: 1511
	SpriteName: "AMON_RA"
	Name: "Amon Ra"
	EggItem: "Amon_Ra_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1785
	SpriteName: "ATROCE"
	Name: "Atroce"
	EggItem: "Atroce_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1039
	SpriteName: "BAPHOMET"
	Name: "Baphomet"
	EggItem: "Baphomet_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1272
	SpriteName: "DARK_LORD"
	Name: "Dark Lord"
	EggItem: "Dark_Lord_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1046
	SpriteName: "DOPPELGANGER"
	Name: "Doppelganger"
	EggItem: "Doppelganger_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1389
	SpriteName: "DRACULA"
	Name: "Dracula"
	EggItem: "Dracula_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1112
	SpriteName: "DRAKE"
	Name: "Drake"
	EggItem: "Drake_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1115
	SpriteName: "EDDGA"
	Name: "Eddga"
	EggItem: "Eddga_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1871
	SpriteName: "FALLINGBISHOP"
	Name: "Fallen Bishop"
	EggItem: "Fallen_Bishop_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1252
	SpriteName: "GARM"
	Name: "Garm"
	EggItem: "Garm_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1086
	SpriteName: "GOLDEN_BUG"
	Name: "Golden Thief Bug"
	EggItem: "Golden_Thief_Bug_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1832
	SpriteName: "IFRIT"
	Name: "Ifrit"
	EggItem: "Ifrit_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1492
	SpriteName: "INCANTATION_SAMURAI"
	Name: "Incantation Samura"
	EggItem: "Incantation_Samura_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1734
	SpriteName: "KIEL_"
	Name: "Kiel"
	EggItem: "Kiel_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1251
	SpriteName: "KNIGHT_OF_WINDSTORM"
	Name: "Stormy Knight"
	EggItem: "Stormy_Knight_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1688
	SpriteName: "LADY_TANEE"
	Name: "Lady Tanee"
	EggItem: "Lady_Tanee_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1373
	SpriteName: "LORD_OF_DEATH"
	Name: "Lord of Death"
	EggItem: "Lord_Of_Death_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1147
	SpriteName: "MAYA"
	Name: "Maya"
	EggItem: "Maya_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1059
	SpriteName: "MISTRESS"
	Name: "Mistress"
	EggItem: "Mistress_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1150
	SpriteName: "MOONLIGHT"
	Name: "Moonlight Flower"
	EggItem: "Moonlight_Flower_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1087
	SpriteName: "ORK_HERO"
	Name: "Orc Hero"
	EggItem: "Orc_Hero_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1190
	SpriteName: "ORC_LORD"
	Name: "Orc Lord"
	EggItem: "Orc_Lord_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1038
	SpriteName: "OSIRIS"
	Name: "Osiris"
	EggItem: "Osiris_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1157
	SpriteName: "PHARAOH"
	Name: "Pharoah"
	EggItem: "Pharaoh_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1159
	SpriteName: "PHREEONI"
	Name: "Phreeoni"
	EggItem: "Phreeoni_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1583
	SpriteName: "TAO_GUNKA"
	Name: "Tao Gunka"
	EggItem: "Tao_Gunka_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1708
	SpriteName: "THANATOS"
	Name: "Thanatos"
	EggItem: "Thanatos_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1312
	SpriteName: "TURTLE_GENERAL"
	Name: "Turtle General"
	EggItem: "Turtle_General_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1751
	SpriteName: "RANDGRIS"
	Name: "Valkyrie Randgris"
	EggItem: "Valkyrie_Randgris_Egg"
	FoodItem: "Rotten_Meat"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1096
	SpriteName: "ANGELING"
	Name: "Angeling"
	EggItem: "Angeling_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1388
	SpriteName: "ARCHANGELING"
	Name: "Arc Angeling"
	EggItem: "Arc_Angeling_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1415
	SpriteName: "BABY_LEOPARD"
	Name: "Baby Leopard"
	EggItem: "Baby_Leopard_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1307
	SpriteName: "CAT_O_NINE_TAIL"
	Name: "Cat'o Nine Tail"
	EggItem: "Cat_o_Nine_Tail_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1198
	SpriteName: "DARK_PRIEST"
	Name: "Dark Priest"
	EggItem: "Dark_Priest_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1582
	SpriteName: "DEVILING"
	Name: "Deviling"
	EggItem: "Deviling_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1093
	SpriteName: "ECLIPSE"
	Name: "Eclipse"
	EggItem: "Eclipse_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1371
	SpriteName: "FAKE_ANGEL"
	Name: "Fake Angel"
	EggItem: "Fake_Angel_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1391
	SpriteName: "GALAPAGO"
	Name: "Galapago"
	EggItem: "Galapago_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1515
	SpriteName: "GARM_BABY"
	Name: "Garm Baby"
	EggItem: "Garm_Baby_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1120
	SpriteName: "GHOSTRING"
	Name: "Ghostring"
	EggItem: "Ghostring_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1989
	SpriteName: "HILLSRION"
	Name: "Hillsrion"
	EggItem: "Hillsrion_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1090
	SpriteName: "MASTERING"
	Name: "Mastering"
	EggItem: "Mastering_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1180
	SpriteName: "NINE_TAIL"
	Name: "Nine Tail"
	EggItem: "Nine_Tail_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 2021
	SpriteName: "PHYLLA"
	Name: "Phylla"
	EggItem: "Phylla_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1930
	SpriteName: "PIAMETTE"
	Name: "Piamette"
	EggItem: "Piamette_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1894
	SpriteName: "POURING"
	Name: "Pouring"
	EggItem: "Pouring_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1776
	SpriteName: "SIROMA"
	Name: "Siroma"
	EggItem: "Siroma_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
{
	Id: 1622
	SpriteName: "TEDDY_BEAR"
	Name: "Teddy"
	EggItem: "Teddy_Bear_Egg"
	FoodItem: "Pet_Food"
	FoodEffectiveness: 80
	HungerDelay: 60
	Intimacy: {
		Initial: 250
		FeedIncrement: 10
		OverFeedDecrement: 100
		OwnerDeathDecrement: 20
	}
	CaptureRate: 1
	Speed: 150
},
)
